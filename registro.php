<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
<title>Prueba ZINOBE PHP</title>
		
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="js/jquery-1.12.4-jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 20px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
</style>
</head>
	<body>
<?php
require_once('CustomerData.php');

$curlService = new CustomerData();

$countries = $curlService->getContries();

require_once "DBconect.php";
session_start();
if(isset($_SESSION["admin_login"]))	//valida si ya esta logueado
{
	header("location: admin/admin_portada.php");	
}

if(isset($_REQUEST['btn_register'])) //compruebe el nombre del botón "btn_register" y configúrelo
{
	$name		= $_REQUEST['txt_username'];	//input nombre "txt_username"
	$document	= $_REQUEST['txt_document'];	//input nombre "txt_document"
	$email		= $_REQUEST['txt_email'];	//input nombre "txt_email"
	$password	= $_REQUEST['txt_password'];	//input nombre "txt_password"
	$country	= $_REQUEST['txt_country'];	//seleccion nombre "txt_country"
		
	if(empty($name)){
		$errorMsg[]="Ingrese nombre";	//Compruebe input nombre de usuario no vacío
	}
	else if(strlen($name) < 3){
		$errorMsg[] = "Nombre minimo 3 caracteres";	//Revisar name 3 caracteres
	}
	else if(empty($document)){
		$errorMsg[]="Ingrese documento";	//Compruebe input nombre de usuario no vacío
	}
	else if(empty($email)){
		$errorMsg[]="Ingrese email";	//Revisar email input no vacio
	}
	else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$errorMsg[]="Ingrese email valido";	//Verificar formato de email
	}
	else if(empty($password)){
		$errorMsg[]="Ingrese password";	//Revisar password vacio o nulo
	}
	else if(strlen($password) < 6){
		$errorMsg[] = "Password minimo 6 caracteres";	//Revisar password 6 caracteres
	}
	else if(preg_match('/\d/', $password) == 0){
		$errorMsg[] = "Password debe tener al menos un digito";	//Revisar password alfanumerico
	}
	else if(empty($country)){
		$errorMsg[] = "Seleccione país";	//Revisar etiqueta select vacio
	}
	else
	{	
		try
		{	
			$select_stmt = $db->prepare("SELECT document, email FROM user 
										WHERE document=:udocument OR email=:uemail"); // consulta sql
			$select_stmt->bindParam(":udocument",$document);  
			$select_stmt->bindParam(":uemail",$email);      //parámetros de enlace
			$select_stmt->execute();
			$row = $select_stmt->fetch(PDO::FETCH_ASSOC);	
			if($row["document"] == $document){
				$errorMsg[] = "Este documento ya existe";	//Verificar documento existente
			}
			else if($row["email"] == $email){
				$errorMsg[] = "Este email ya existe";	//Verificar email existente
			}
			
			else if(!isset($errorMsg))
			{
				$password_encrypt = base64_encode($password);	
				$insert_stmt = $db->prepare("INSERT INTO user(name,document,email,country,password) VALUES(:uname,:udocument,:uemail,:ucountry,:upassword)"); //Consulta sql de insertar			
				$insert_stmt->bindParam(":uname",$name);	
				$insert_stmt->bindParam(":udocument",$document);	
				$insert_stmt->bindParam(":uemail",$email);	  		//parámetros de enlace 
				$insert_stmt->bindParam(":ucountry",$country);
				$insert_stmt->bindParam(":upassword",$password_encrypt);
				
				if($insert_stmt->execute())
				{
					$registerMsg = "Registro exitoso: Esperar página de inicio de sesión"; //Ejecuta consultas 
					header("refresh:2;index.php"); //Actualizar despues de 2 segundo a la portada
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
}
include("header.php");
?>
	<div class="wrapper">	
	<div class="container">	
		<div class="col-lg-12">
		
		<?php
		if(isset($errorMsg))
		{
			foreach($errorMsg as $error)
			{
			?>
				<div class="alert alert-danger">
					<strong>INCORRECTO ! <?php echo $error; ?></strong>
				</div>
            <?php
			}
		}
		if(isset($registerMsg))
		{
		?>
			<div class="alert alert-success">
				<strong>EXITO ! <?php echo $registerMsg; ?></strong>
			</div>
        <?php
		}
		?> 
<div class="login-form">  
<center><h2>Registro</h2></center>
<form method="post" class="form-horizontal">
    
<div class="form-group">
	<label class="col-sm-9 text-left">Name</label>
	<div class="col-sm-12">
		<input type="text" name="txt_username" class="form-control" placeholder="Ingrese usuario" />
	</div>
</div>

<div class="form-group">
	<label class="col-sm-9 text-left">Document</label>
	<div class="col-sm-12">
		<input type="text" name="txt_document" class="form-control" placeholder="Ingrese Documento" />
	</div>
</div>

<div class="form-group">
	<label class="col-sm-9 text-left">Email</label>
	<div class="col-sm-12">
		<input type="text" name="txt_email" class="form-control" placeholder="Ingrese email" />
	</div>
</div>
    
<div class="form-group">
	<label class="col-sm-9 text-left">Password</label>
	<div class="col-sm-12">
		<input type="password" name="txt_password" class="form-control" placeholder="Ingrese password" />
	</div>
</div>
    
<div class="form-group">
    <label class="col-sm-9 text-left">País</label>
    <div class="col-sm-12">
    <select class="form-control" name="txt_country">
        <option value="" selected="selected"> - seleccione país - </option>
        <?php
        foreach ($countries as $country) : ?>
        <option value="<?php echo $country->name; ?>"><?php echo $country->name; ?></option>
       <?php endforeach  ?>
    </select>
    </div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<input type="submit" name="btn_register" class="btn btn-primary btn-block" value="Registro">
		<!-- <a href="index.php" class="btn btn-danger">Cancel</a> -->
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
	¿Tienes una cuenta? <a href="index.php"><p class="text-info">Inicio de sesión</p></a>		
	</div>
</div>
    
</form>
</div><!--Cierra div login-->
		</div>
	</div>		
	</div>									
	</body>
</html>