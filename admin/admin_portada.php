<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
<title>Prueba ZINOBE PHP</title>
		
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<script src="../js/jquery-1.12.4-jquery.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 20px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
</style>
</head>
	<body>
<?php 
require_once('../CustomerData.php');
include("../header.php");
?>	
	<div class="wrapper">
	<div class="container">	
		<div class="col-lg-12">
			<center>
				<h1>Pagina Administrativa</h1>
				
				<h3>
				<?php
				session_start();

				if(!isset($_SESSION['admin_login']))	
				{
					header("location: ../index.php");  
				}
				
				if(isset($_SESSION['admin_login']))
				{
				?>
					Bienvenido,
				<?php
						echo $_SESSION['admin_login'];
				}

				if(isset($_REQUEST['btn_search'])) //compruebe el nombre del botón "btn_search" y configúrelo
				{
					$search	= $_REQUEST['txt_search'];	//input nombre "txt_search"
						
					if(empty($search)){
						$errorMsg[]="Ingrese nombre o email";	//Compruebe input search no vacío
					} else
					{	
						$searchServices = new CustomerData();

						$search = $searchServices->getUserDirectory($search,$search);

						if(empty($search)){
							$errorMsg[] = "No se encontraron resultados";
						} else {
							try
							{	
								$select_stmt = $db->prepare("SELECT document, email FROM user 
															WHERE document=:udocument OR email=:uemail"); // consulta sql
								$select_stmt->bindParam(":udocument",$search['document']);  
								$select_stmt->bindParam(":uemail",$search['email']);      //parámetros de enlace
								$select_stmt->execute();
								$row = $select_stmt->fetch(PDO::FETCH_ASSOC);	
								if($row["document"] == $search["document"]){
									$errorMsg[] = "Este documento ya existe";	//Verificar documento existente
								}
								else if($row["email"] == $search["email"]){
									$errorMsg[] = "Este email ya existe";	//Verificar email existente
								}
								
								else if(!isset($errorMsg))
								{	
									$insert_stmt = $db->prepare("INSERT INTO user(name,document,email,country,password) VALUES(:uname,:udocument,:uemail,:ucountry,:upassword)"); //Consulta sql de insertar			
									$insert_stmt->bindParam(":uname",$search['name']);	
									$insert_stmt->bindParam(":udocument",$search['document']);	
									$insert_stmt->bindParam(":uemail",$search['email']);	  		//parámetros de enlace 
									$insert_stmt->bindParam(":ucountry",$search['country']);
									$insert_stmt->bindParam(":upassword",$search['password']);
									
									if($insert_stmt->execute())
									{
										$registerMsg = "Usuario encontrado y registrado";
										header("refresh:3;admin_portada.php");
									}
								}
							}
							catch(PDOException $e)
							{
								echo $e->getMessage();
							}
						}
					}
				}
				?>
				</h3>
					
			</center>
			<a href="../cerrar_sesion.php"><button class="btn btn-danger text-left"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Cerrar Sesion</button></a>
            <hr>
            		<?php
            		if(isset($errorMsg))
            		{
            			foreach($errorMsg as $error)
            			{
            			?>
            				<div class="alert alert-danger">
            					<strong>INCORRECTO ! <?php echo $error; ?></strong>
            				</div>
                        <?php
            			}
            		}
            		if(isset($registerMsg))
            		{
            		?>
            			<div class="alert alert-success">
            				<strong>EXITO ! <?php echo $registerMsg; ?></strong>
            			</div>
                    <?php
            		}
            		?> 
            <center><h2>Búsqueda</h2></center>
            <form method="post" class="form-horizontal">    
	            <div class="form-group">
	            	<label class="col-sm-9 text-left">Buscar</label>
	            	<div class="col-sm-12">
	            		<input type="text" name="txt_search" class="form-control" placeholder="Busca por nombre o email" />
	            	</div>
	            </div>
	            <div class="form-group">
	            	<div class="col-sm-12">
	            		<input type="submit" name="btn_search" class="btn btn-primary btn-block" value="Buscar">
	            		<!-- <a href="index.php" class="btn btn-danger">Cancel</a> -->
	            	</div>
	            </div>
            </form>
		</div>
		
		<br><br><br>
		<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Panel de usuarios
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th width="4%">ID</th>
                                            <th width="18%">Nombre</th>
                                            <th width="18%">Documento</th>
                                            <th width="24%">Email</th>
                                            <th width="19%">Pais</th>
                                            <th width="24%">Password</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									require_once '../DBconect.php';
									$select_stmt=$db->prepare("SELECT id,name,document,email,country FROM user");
									$select_stmt->execute();
									
									while($row=$select_stmt->fetch(PDO::FETCH_ASSOC))
									{
									?>
                                        <tr>
                                            <td><?php echo $row["id"]; ?></td>
                                            <td><?php echo $row["name"]; ?></td>
                                            <td><?php echo $row["document"]; ?></td>
                                            <td><?php echo $row["email"]; ?></td>
                                            <td><?php echo $row["country"]; ?></td>
                                            <td>*******</td>
                                        </tr>
									<?php 
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>	
	</div>		
	</div>								
	</body>
</html>