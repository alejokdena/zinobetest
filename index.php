<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
<title>Prueba ZINOBE PHP</title>
		
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="js/jquery-1.12.4-jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 20px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {        
        font-size: 15px;
        font-weight: bold;
    }
</style>
</head>
	<body>
<?php
require_once 'DBconect.php';
session_start();
if(isset($_SESSION["admin_login"]))	//valida si ya esta logueado
{
	header("location: admin/admin_portada.php");	
}

if(isset($_REQUEST['btn_login']))	
{
	$email		= $_REQUEST["txt_email"];	//textbox nombre "txt_email"
	$password	= $_REQUEST["txt_password"];	//textbox nombre "txt_password"
		
	if(empty($email)){						
		$errorMsg[] = "Por favor ingrese Email";	//Revisar email
	}
	else if(empty($password)){
		$errorMsg[] = "Por favor ingrese Password";	//Revisar password vacio
	}
	else if($email AND $password)
	{
		$password = base64_encode($password);
		try
		{
			$select_stmt = $db->prepare("SELECT email,password FROM user
										WHERE
										email=:uemail AND password=:upassword"); 
			$select_stmt->bindParam(":uemail",$email);
			$select_stmt->bindParam(":upassword",$password);
			$select_stmt->execute();	//execute query
					
			while($row = $select_stmt->fetch(PDO::FETCH_ASSOC))	
			{
				$dbemail	= $row["email"];
				$dbpassword	= $row["password"];
			}
			if($email != null AND $password != null)	
			{
				if($select_stmt->rowCount() > 0)
				{
					if($email == $dbemail and $password == $dbpassword)
					{
						$_SESSION["admin_login"] = $email;			
						$loginMsg="Admin: Inicio sesión con éxito";	
						header("refresh:3;admin/admin_portada.php");	
					} else {
						$errorMsg[]="correo electrónico o contraseña incorrectos";
					}
				}
				else
				{
					var_dump("aca");
					die();
					$errorMsg[]="correo electrónico o contraseña incorrectos";
				}
			}
			else
			{
				$errorMsg[]="correo electrónico o contraseña incorrectos";
			}
		}
		catch(PDOException $e)
		{
			$e->getMessage();
		}		
	}
	else
	{
		$errorMsg[]="correo electrónico o contraseña incorrectos";
	}
}
include("header.php");
?>
	<div class="wrapper">	
	<div class="container">		
		<div class="col-lg-12">
		
		<?php
		if(isset($errorMsg))
		{
			foreach($errorMsg as $error)
			{
			?>
				<div class="alert alert-danger">
					<strong><?php echo $error; ?></strong>
				</div>
            <?php
			}
		}
		if(isset($loginMsg))
		{
		?>
			<div class="alert alert-success">
				<strong>ÉXITO ! <?php echo $loginMsg; ?></strong>
			</div>
        <?php
		}
		?> 

<div class="login-form">
<center><h2>Iniciar sesión</h2></center>
<form method="post" class="form-horizontal">
  <div class="form-group">
  	<label class="col-sm-6 text-left">Email</label>
	<div class="col-sm-12">
		<input type="text" name="txt_email" class="form-control" placeholder="Ingrese email" />
	</div>
  </div>
      
  <div class="form-group">
	<label class="col-sm-6 text-left">Password</label>
	<div class="col-sm-12">
		<input type="password" name="txt_password" class="form-control" placeholder="Ingrese passowrd" />
	</div>
  </div>
      
  <div class="form-group">
	<div class="col-sm-12">
		<input type="submit" name="btn_login" class="btn btn-success btn-block" value="Iniciar Sesion">
	</div>
  </div>
  
  <div class="form-group">
	<div class="col-sm-12">
	¿No tienes una cuenta? <a href="registro.php"><p class="text-info">Registrar Cuenta</p></a>		
	</div>
  </div>     
</form>
</div>
<!--Cierra div login-->
		</div>	
	</div>		
	</div>									
	</body>
</html>