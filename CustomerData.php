<?php
require_once('vendor/autoload.php');
require_once "DBconect.php";

use Ixudra\Curl\CurlService;

class CustomerData{
	public function getContries()
	{
		$curlService = new CurlService();
		$countries = $curlService->to('https://restcountries.eu/rest/v2/all')
		    ->asJsonResponse()
		    ->get();

		return $countries;
	}
	public function getUserDirectory($name, $email)
	{
		$curlService = new CurlService();
		$users = $curlService->to('http://www.mocky.io/v2/5d9f39263000005d005246ae?mocky-delay=10s')
		    ->asJsonResponse()
		    ->get();
		$user = [];
		foreach ($users->objects as $value) {
			if($value->first_name == $name || $value->email == $email){
				$user = ['name' => $value->first_name.' '.$value->last_name,
						'document' => $value->document,
						'email' => $value->email,
						'country' => $value->country,
						'password' => base64_encode('temp123')];
			}
		}
		return $user;
	}
}
?>